from app.service.servicebase import ServiceBase
from app.service.serviceresponses import GetAllApiResponse, CalculateInsuranceResponse, \
    GetDashboardDataResponse, GetCarInsuranceDetailsResponse
from app.service.serviceviews import CalculatedInsuranceView, DashboardView, CarDetailsView, CarInsuranceDetailsView
from app.common.util import MessageType, PremiumType, UuidValidator
from app.models import ApiResponse, Car, InsuranceAddition, ApiRequest, InsuranceDetail
from app.helpers.insurancecalculator import InsuranceCalculator
import uuid


class AppService(ServiceBase):

    def get_api_response(self):
        response = GetAllApiResponse()
        api_responses = ApiResponse.objects.filter(is_active=True)

        response.ApiResponses = api_responses
        response.message_type = MessageType.Success
        response.message = 'Api responses loaded successfully'
        return response

    def calculate_insurance(self, premium_type, car_id):
        #instantiate service reponse class
        response = CalculateInsuranceResponse()

        #save request for this api call with the time
        api_request = ApiRequest()
        api_request.request_id = uuid.uuid4()
        api_request.type = premium_type
        api_request.car_id = car_id
        api_request.save()

        #validate request parameters if not exit process and return response
        if premium_type == '' or car_id == '':
            response.message_type = MessageType.Error
            response.message = "Invalid parameter passed"
            return response

        car_id = car_id.replace("-", "")

        #check if car_id is a valid uuid if not exit process and return response
        if not UuidValidator.validate_uuid4(car_id):
            response.message_type = MessageType.Error
            response.message = "Invalid car id provided"
            return response

        #Load car details from db
        car = Car.objects.filter(car_id=car_id).first()

        #check if car exists if not exit process and return reponse
        if car is None:
            response.message_type = MessageType.Info
            response.message = "Car was not found"
            return response

        #load insurance additions (Contants) from database for premium calculation
        insurance_additions = InsuranceAddition.objects.filter(is_active=True).first()

        #instantiate required classes for calculation
        calculated_view = CalculatedInsuranceView()
        insurance_calculator = InsuranceCalculator()

        #check and call method for insurance calculation based on premium type provided
        if premium_type.lower().strip() == PremiumType.ThirdParty.lower():
            insurance_price = insurance_calculator.calculate_third_party_premium(car.insurance, insurance_additions)
        elif premium_type.lower().strip() == PremiumType.Comprehensive.lower():
            insurance_price = insurance_calculator.calculate_comprehensive_premium(car.insurance, insurance_additions)
        else:
            response.message_type = MessageType.Error
            response.message = "Unknown Insurance premium type"
            return response

        calculated_view.type = premium_type
        calculated_view.insurance_price = insurance_price
        calculated_view.car_id = car_id

        #save response for this call with the assocaited request id
        api_response = ApiResponse()
        api_response.car_id = car_id
        api_response.type = premium_type
        api_response.insurance_price = insurance_price
        api_response.request_id = str(api_request.request_id).replace("-", "")
        api_response.save()

        #map data to service response object
        response.CalculatedInsuranceView = calculated_view.__dict__
        response.message_type = MessageType.Success
        response.message = premium_type + " insurance premium calculated successfully"

        #return service response
        return response

    def get_dashboard_data(self):
        #instantiate service response class
        response = GetDashboardDataResponse()
        dashboard_view = DashboardView()

        #get count of  api requests and responses
        dashboard_view.no_of_requests = ApiRequest.objects.filter(is_active=True).count()
        dashboard_view.no_of_responses = ApiResponse.objects.filter(is_active=True).count()

        #get insurance additons for calculating premiums
        insurance_addition = InsuranceAddition.objects.filter(is_active=True).first()
        if insurance_addition is not None:
            dashboard_view.additional_peril = insurance_addition.additional_peril
            dashboard_view.ecowas_peril = insurance_addition.ecowas_peril
            dashboard_view.pa_benefits = insurance_addition.pa_benefits
            dashboard_view.nic_nhis_ecowas = insurance_addition.nic_nhis_ecowas

        #get count of comprehensives and third parties
        dashboard_view.no_of_comprehensives = InsuranceDetail.objects.filter(insurance_type=PremiumType.Comprehensive).count()
        dashboard_view.no_of_third_parties = InsuranceDetail.objects.filter(insurance_type=PremiumType.ThirdParty).count()

        #load top 150 latest added cars
        cars = Car.objects.filter(is_active=True).order_by('-created_on')[:150]
        dashboard_view.cars = []

         #Check if there are any cars issured if yes loop and map to view object
        if cars is not None:
            for car in cars:
                car_view = CarDetailsView()
                car_view.car_id = str(car.car_id).replace("-", "")
                car_view.car_model = car.vehicle_make
                car_view.cc = car.vehicle_cc
                car_view.registration_no = car.registration_no
                car_view.insurance_type = car.insurance.insurance_type
                car_view.chassis = car.chassis
                car_view.year = car.year
                dashboard_view.cars.append(car_view)

        #get total number of insured cars
        dashboard_view.no_of_insured_cars = dashboard_view.no_of_comprehensives + dashboard_view.no_of_third_parties

        #map data to service response object
        response.DashboardView = dashboard_view
        response.message = "Dashboard data loaded successfully"
        response.message_type = MessageType.Success

        #return service response
        return response

    def get_car_insurance_details(self, car_id):
        #instantiate service response class
        response = GetCarInsuranceDetailsResponse()
        car_details_view = CarInsuranceDetailsView()

        #Load car details from db
        car = Car.objects.filter(car_id=car_id).first()

        #Map model to service view class
        if car is not None:
            car_details_view.car_id = car.car_id
            car_details_view.car_model = car.vehicle_make
            car_details_view.cc = car.vehicle_cc
            car_details_view.chassis = car.chassis
            car_details_view.year = car.year
            car_details_view.registration_no = car.registration_no
            car_details_view.insurance_type = car.insurance.insurance_type
            car_details_view.insured_by = car.insured_by
            car_details_view.address = car.address
            car_details_view.contact_no = car.contact_no
            car_details_view.vehicle_cc = car.vehicle_cc
            car_details_view.engine_no = car.engine_no
            car_details_view.no_of_seats = car.no_of_seats
            car_details_view.licence_issue_year = car.licence_issue_year
            car_details_view.sum_insured = car.insurance.sum_insured
            car_details_view.own_damage_rate = car.insurance.own_damage_rate
            car_details_view.cc_loadings = car.insurance.cc_loadings
            car_details_view.old_age_loadings = car.insurance.old_age_loadings
            car_details_view.inexperienced_driver_loading = car.insurance.inexperienced_driver_loading
            car_details_view.tp_basic_premium = car.insurance.tp_basic_premium
            car_details_view.extra_seat = car.insurance.extra_seat
            car_details_view.tppdl = car.insurance.tppdl
            car_details_view.ncd = car.insurance.ncd
            car_details_view.fleet_discount = car.insurance.fleet_discount
            car_details_view.excess_brought_back = car.insurance.excess_brought_back
            car_details_view.extra_tppd_cover = car.insurance.extra_tppd_cover

        #map data to service response object
        response.CarInsuranceDetailsView = car_details_view
        response.message = "Car details loaded successfully"
        response.message_type = MessageType.Success

        #return service response
        return response

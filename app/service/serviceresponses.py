#specialize classes for returning response from the service layer


class ServiceResponse:
    message_type = ''
    message = ''


class GetAllApiResponse(ServiceResponse):
    ApiResponses = None


class CalculateInsuranceResponse(ServiceResponse):
    CalculatedInsuranceView = None


class GetDashboardDataResponse(ServiceResponse):
    DashboardView = None


class GetCarInsuranceDetailsResponse(ServiceResponse):
    CarInsuranceDetailsView = None



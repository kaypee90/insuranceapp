from rest_framework import viewsets

from rest_framework import status
from app.common.util import MessageType
from rest_framework.response import Response
from .serializers import ApiResponseSerializer
from app.service.appservice import AppService
from app.common.logging import LogHelper

import logging
log = logging.getLogger(__name__)


class InsuranceViewSet(viewsets.ModelViewSet):
    _as = AppService()
    _response = _as.get_api_response()
    serializer_class = ApiResponseSerializer
    queryset = _response.ApiResponses

    def create(self, request):
        #log api request with its ip
        log.info("Api request made from ip: {}".format(LogHelper.get_ip_from_request(request)))

        api_response = None
        try:
            #retrieve data from query string
            premium_type = request.data['type']
            car_id = request.data['car_id']


            #accessing data from the service layer
            response = self._as.calculate_insurance(premium_type, car_id)

            #get message from service layer for logging
            log_message = "{} for ip: {}".format(response.message, LogHelper.get_ip_from_request(request))

            #map reponse from service layer
            if response.message_type == MessageType.Success:
                status_code = status.HTTP_200_OK
                api_response = self.serializer_class.serialize(self.serializer_class, response.CalculatedInsuranceView)
                response_status = 'Ok'
                log.info(log_message)
                response_message = response.message
            elif response.message_type == MessageType.Info:
                status_code = status.HTTP_404_NOT_FOUND
                response_status = 'Not Found'
                response_message = response.message
                log.info(log_message)
            else:
                status_code = status.HTTP_400_BAD_REQUEST
                response_status = 'Bad Request'
                response_message = response.message
                log.warning(log_message)
        except Exception as ex:
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            response_status = 'Internal Server Error'
            response_message = 'Insurance calculation failed'
            log.error("{} - ip {}".format(str(ex), LogHelper.get_ip_from_request(request)))

        #return json response wuth status code
        return Response({'data': api_response, 'status': response_status, 'message': response_message}, status=status_code)




from rest_framework import serializers
from app.models import ApiResponse
from app.api.baseserializer import BaseSerializer


class ApiResponseSerializer(serializers.ModelSerializer, BaseSerializer):
    type = serializers.CharField(required=True)
    car_id = serializers.CharField()

    class Meta:
        model = ApiResponse
        fields = ('type', 'car_id')

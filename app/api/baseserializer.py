from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.utils.six import BytesIO
import json


class BaseSerializer:
    def serialize(self, data):
        return JSONRenderer().render(data)

    def deserialize(self, content):
        stream = BytesIO(content)
        return JSONParser().parse(stream)


from uuid import UUID


class MessageType:
    Success = 'success'
    Info = 'info'
    Error = 'error'
    Warning = 'warning'


class PremiumType:
    ThirdParty = 'Third Party'
    Comprehensive = 'Comprehensive'


class UuidValidator:
    def validate_uuid4(uuid_string):
        try:
            val = UUID(uuid_string, version=4)
        except ValueError:
        # If it's a value error, then the string
        # is not a valid hex code for a UUID.
            return False

        # If the uuid_string is a valid hex code,
        # but an invalid uuid4,
        # the UUID.__init__ will convert it to a
        #valid uuid4. This is bad for validation purposes.
        return val.hex == uuid_string

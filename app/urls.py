from django.conf.urls import url, include
from app.api.router import router
from app import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^car/(?P<car_id>\w+)', views.car_details, name='car_details'),
    url(r'^api/', include(router.urls)),
]

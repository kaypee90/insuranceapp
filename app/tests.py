from django.test import TestCase
from app.service.appservice import AppService
from app.common.util import MessageType, PremiumType


class AppServiceTests(TestCase):
    _as = None

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        self._as = AppService()

    def test_message_type_for_calculate_insurance_with_empty_parameters(self):
        response = self._as.calculate_insurance("", "")
        self.assertEqual(response.message_type, MessageType.Error)

    def test_message_calculate_for_insurance_with_empty_parameters(self):
        response = self._as.calculate_insurance("", "")
        self.assertEquals(response.message, "Invalid parameter passed")

    def test_message_type_for_calculate_insurance_with_one_parameters(self):
        response = self._as.calculate_insurance("", "60be81bc3d4a4870bbf3ddb44f38e3f0")
        self.assertEqual(response.message_type, MessageType.Error)

    def test_message_for_calculate_insurance_with_one_parameters(self):
        response = self._as.calculate_insurance(PremiumType.ThirdParty, "")
        self.assertEquals(response.message, "Invalid parameter passed")

    def test_message_type_for_calculate_insurance_with_incorrect_car_id(self):
        response = self._as.calculate_insurance(PremiumType.Comprehensive, "50be87bc3d4a4870bbf3ddb44f38e3f0")
        self.assertEquals(response.message_type, MessageType.Info)

    def test_message_for_calculate_insurance_with_incorrect_car_id(self):
        response = self._as.calculate_insurance(PremiumType.Comprehensive, "50be87bc3d4a4870bbf3ddb44f38e3f0")
        self.assertEquals(response.message, "Car was not found")

    def test_message_type_for_calculate_insurance_with_invalid_car_id_format(self):
        response = self._as.calculate_insurance(PremiumType.Comprehensive, "fgsfdgsdfgsdfgwrtqtwrtwert")
        self.assertEquals(response.message_type, MessageType.Error)

    def test_message_for_calculate_insurance_with_invalid_car_id_format(self):
        response = self._as.calculate_insurance(PremiumType.Comprehensive, "asdfasdasgas454sagasfgsafga")
        self.assertEquals(response.message, "Invalid car id provided")

    def test_message_type_for_calculate_insurance_with_invalid_premium_type(self):
        response = self._as.calculate_insurance("Third Party Premium", "60be81bc3d4a4870bbf3ddb44f38e3f1")
        self.assertEquals(response.message_type, MessageType.Error)

    def test_message_for_get_dashboard_data(self):
        response = self._as.get_dashboard_data()
        self.assertEquals(response.message, "Dashboard data loaded successfully")

    def test_message_type_for_get_dashboard_data(self):
        response = self._as.get_dashboard_data()
        self.assertEquals(response.message_type, MessageType.Success)

    def test_cars_response_for_get_dashboard_data(self):
        response = self._as.get_dashboard_data()
        self.assertFalse(response.DashboardView.cars, None)

    def test_message_for_get_car_insurance_details_with_valid_car_id(self):
        response = self._as.get_car_insurance_details("60be81bc3d4a4870bbf3ddb44f38e3f1")
        self.assertEquals(response.message, "Car details loaded successfully")

    def test_message_type_for_get_car_insurance_details_with_valid_car_id(self):
        response = self._as.get_car_insurance_details("60be81bc3d4a4870bbf3ddb44f38e3f1")
        self.assertEquals(response.message_type, MessageType.Success)





